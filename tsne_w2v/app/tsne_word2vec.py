from MulticoreTSNE import MulticoreTSNE as TSNE
from numpy import array
import os
import pickle
from app.create_filename_tsne import create_filename

cur_work_dir = os.getcwd()


def label_coordinate_mapping(config, master_path, labels, new_values):
    mapping = dict()
    for i in range(len(new_values)):
        mapping[labels[i]] = new_values[i]
    mapping_name = "mapping" + create_filename(config) + ".pickle"
    with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                           config['tsne']['plot_save_path'], "./mapping/", mapping_name)),
              'wb') as handle:
        pickle.dump(mapping, handle, protocol=pickle.HIGHEST_PROTOCOL)

    return mapping


def tsne_plot(config, master_path, model):
    """Creates and TSNE model and plots it"""
    labels = []
    tokens = []

    ft_words = model.wv.vocab
    print(len(ft_words))
    for i, word in enumerate(ft_words):
        tokens.append(model.wv.get_vector(word))
        labels.append(word)

    tsne_token = array(tokens)
    tsne_model = TSNE(n_jobs=config['tsne']['cores'], perplexity=config['tsne']['perplexity'],
                      n_components=config['tsne']['n_components'], init='random', n_iter=config['tsne']['iter'],
                      random_state=23)
    new_values = tsne_model.fit_transform(tsne_token)

    new_values = new_values.tolist()
    print(len(tokens))
    print(len(labels))
    print(len(new_values))

    # label_coordinate_mapping stores a pickle file containing blockid's(labels) mapped to their 2-D coordinate,
    # comment it out if the pickle file is already present
    mapping = label_coordinate_mapping(config, master_path, labels, new_values)
    print("Mapping generated")
    return mapping
