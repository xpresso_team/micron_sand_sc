import time
import json
import os
import gensim
from gensim.models import Word2Vec
from gensim.models.callbacks import CallbackAny2Vec

from app.tsne_word2vec import tsne_plot
from app.model_name_gen import create_modelname
from app.create_filename_tsne import create_filename

import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('Agg')


cur_work_dir = os.getcwd()
config = json.load(open(os.path.abspath(os.path.join(cur_work_dir, 'config/config.json'))))
filename = config['data_preprocess']['input_file'].split('/')[-1].split('.')[0]
master_path = config['data_preprocess']['master_path']
master_path = master_path.replace("FILE", filename)
master_path = master_path.replace("CUTTING", str(config['data_preprocess']['cutting_window']))
master_path = master_path.replace("SPLIT", str(config['data_preprocess']['train_split_ratio']))
master_path = master_path.replace("RW", str(config['data_preprocess']['use_read_write_operation_feature']))


class EpochSaver(CallbackAny2Vec):
    """Callback to save model after each epoch and show training parameters """

    def __init__(self, savedir):
        self.savedir = savedir
        self.epoch = 0
        os.makedirs(self.savedir, exist_ok=True)

    def on_epoch_end(self, model):
        savepath = os.path.join(self.savedir, "model_neg{}_epoch.gz".format(self.epoch))
        model.save(savepath)
        print(
            "Epoch saved: {}".format(self.epoch + 1),
            "Start next epoch ... ", sep="\n"
        )
        if os.path.isfile(os.path.join(self.savedir, "model_neg{}_epoch.gz".format(self.epoch - 1))):
            print("Previous model deleted ")
            os.remove(os.path.join(self.savedir, "model_neg{}_epoch.gz".format(self.epoch - 1)))
        self.epoch += 1


def load_word2vec_model(CallbackAny2Vec):
    """Callback to save model after each epoch and show training parameters """

    def __init__(self):
        self.epoch = 0

    def on_epoch_end(self, model):
        # print("Epoch saved: {}".format(self.epoch + 1),"Start next epoch ... ", sep="\n")
        self.epoch += 1
        # print("Model loss:", (model.get_latest_training_loss() / self.epoch))


def read_write(mapping, read_write_bool):
    if read_write_bool:
        x_read = list()
        y_read = list()
        x_write = list()
        y_write = list()
        for value in mapping:
            if value.endswith("0"):
                x_read.append(mapping[value][0])
                y_read.append(mapping[value][1])
            elif value.endswith("1"):
                x_write.append(mapping[value][0])
                y_write.append(mapping[value][1])
        return x_read, y_read, x_write, y_write
    else:
        x = []
        y = []
        for value in mapping:
            x.append(mapping[value][0])
            y.append(mapping[value][1])
        return x, y


if __name__ == "__main__":
    tic = time.time()
    model_name_temp = "model" + create_modelname(config['model']['w2vec']) + ".model"
    model_name = Word2Vec.load(os.path.abspath(
        os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                     str(config['w2vec']['model_save_path']), str(model_name_temp))))

    mapping = tsne_plot(config, master_path, model_name)
    toc = time.time()
    print("Time taken for TSNE in seconds: " + str(toc - tic))

    read_write_bool = config['data_preprocess']['use_read_write_operation_feature']

    if read_write_bool:

        x_read, y_read, x_write, y_write = read_write(mapping, read_write_bool)

        plt.figure(figsize=(16, 16))
        plt.scatter(x_read, y_read, c="b")
        plt.scatter(x_write, y_write, c="r", alpha=0.3)
        plotname = "plot_word2vec_" + create_filename(config) + ".png"
        plt.savefig(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                                 config['tsne']['plot_save_path'], plotname)))

        plt.figure(figsize=(16, 16))
        plt.scatter(x_read, y_read, c="b")
        plotname = "plot_word2vec_" + create_filename(config) + "_read.png"
        plt.savefig(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                                 config['tsne']['plot_save_path'], plotname)))

        plt.figure(figsize=(16, 16))
        plt.scatter(x_write, y_write, c="r")
        plotname = "plot_word2vec_" + create_filename(config) + "_write.png"
        plt.savefig(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                                 config['tsne']['plot_save_path'], plotname)))

    else:
        x, y = read_write(mapping, read_write_bool)
        plt.figure(figsize=(16, 16))
        plt.scatter(x, y, c="b")
        plotname = "plot_word2vec_" + create_filename(config) + ".png"
        plt.savefig(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                                 config['tsne']['plot_save_path'], plotname)))
