from __future__ import print_function
import gensim
import time
import os
import logging
from gensim.models.callbacks import CallbackAny2Vec
import multiprocessing
from app.model_name_gen import create_modelname, get_master_path
from xpresso.ai.core.logging.xpr_log import XprLogger

import json

cur_work_dir = os.getcwd()

config = json.load(open(os.path.abspath(os.path.join(cur_work_dir, 'config/config.json'))))
w2vec_config = config['model']['w2vec']
master_path = get_master_path(config)

''' Creating logger '''
path = os.getcwd()
config_file = os.path.join(path, 'config', 'xpr_log_stage.json')
my_logger = XprLogger(config_path=config_file)

tic = time.time()

my_logger.info('reading sentence file')

file_name = os.path.abspath(
    os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path, './data/train_seq.txt'))
file = open(file_name, 'r')
ocr_removed = file.readlines()

model_name_temp = "model" + create_modelname(w2vec_config) + ".model"
model_name = os.path.abspath(os.path.join(cur_work_dir, str(config['data_preprocess']['mounted_path']), master_path,
                                          str(config['w2vec']['model_save_path']), str(model_name_temp)))

train_file = file_name

# define training data

my_logger.info('converting sentences to word2vec format')

sentenses = list()

for line in ocr_removed:
    words = line.split()
    sentenses.append(words)

my_logger.info("training data build")

toc = time.time()

my_logger.info(f"total data creation time is {str(toc - tic)}")

tic = time.time()


class EpochSaver(CallbackAny2Vec):
    """Callback to save model after each epoch and show training parameters """

    def __init__(self):
        self.epoch = 0

    def on_epoch_end(self, model):
        my_logger.info(f"Epoch saved: {str(self.epoch + 1)} \n Start next epoch ... ")
        self.epoch += 1
        my_logger.info(f"Model loss: {str((model.get_latest_training_loss() / self.epoch))}")
        if self.epoch % 2000 == 0:
            my_logger.info(f"saving model for epoch {str(self.epoch)}.model")
            model.save(f"{model_name}_epoch_ {str(self.epoch)}.model")


my_logger.info("Training word2vec model")

# build vocabulary and train model
model = gensim.models.Word2Vec(
    sentenses,
    size=w2vec_config['size'],
    window=w2vec_config['window'],
    min_count=w2vec_config['min_count'],
    workers=multiprocessing.cpu_count(),
    seed=w2vec_config['seed'],
    sg=w2vec_config['sg'],
    alpha=w2vec_config['alpha'],
    negative=w2vec_config['negative'])

# train model
model.train(sentenses, total_examples=len(sentenses), epochs=w2vec_config['epochs'], compute_loss=True,
            callbacks=[EpochSaver()])

my_logger.info('word2ve model trained')

toc = time.time()
my_logger.info(f"total training time is {str(toc - tic)}")

# save model

tic = time.time()
my_logger.info("saving model")

model.save(model_name)

my_logger.info("model saved")
toc = time.time()
my_logger.info(f"total model saving  time is {str(toc - tic)}")

