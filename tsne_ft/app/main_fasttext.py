import time
import json
import fasttext
import os
from app.tsne_fasttext import tsne_plot
from app.create_filename_tsne import create_filename
from app.model_name_gen import create_modelname
from xpresso.ai.core.logging.xpr_log import XprLogger

import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt

cur_work_dir = os.getcwd()
config = json.load(open(os.path.abspath(os.path.join(cur_work_dir, 'config/config.json'))))
filename = config['data_preprocess']['input_file'].split('/')[-1].split('.')[0]
master_path = config['data_preprocess']['master_path']
master_path = master_path.replace("FILE", filename)
master_path = master_path.replace("CUTTING", str(config['data_preprocess']['cutting_window']))
master_path = master_path.replace("SPLIT", str(config['data_preprocess']['train_split_ratio']))
master_path = master_path.replace("RW", str(config['data_preprocess']['use_read_write_operation_feature']))


def read_write(mapping, read_write_bool):
    if read_write_bool:
        x_read = list()
        y_read = list()
        x_write = list()
        y_write = list()
        for value in mapping:
            if value.endswith("0"):
                x_read.append(mapping[value][0])
                y_read.append(mapping[value][1])
            elif value.endswith("1"):
                x_write.append(mapping[value][0])
                y_write.append(mapping[value][1])
        return x_read, y_read, x_write, y_write
    else:
        x = []
        y = []
        for value in mapping:
            x.append(mapping[value][0])
            y.append(mapping[value][1])
        return x, y


if __name__ == "__main__":
    tic = time.time()

    path = os.getcwd()
    config_file = os.path.join(path, 'config', 'xpr_log_stage.json')
    my_logger = XprLogger(config_path=config_file)

    my_logger.info("Loading Fasttext model...")
    model_name_temp = "model_" + create_modelname(config['model']['fasttext']) + ".bin"
    model_name = fasttext.load_model(os.path.abspath(
        os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                     str(config['fasttext']['model_save_path']), str(model_name_temp))))
    my_logger.info("Model loaded")

    mapping = tsne_plot(config, master_path, model_name, my_logger)

    my_logger.info("Plotting TSNE results")

    read_write_bool = config['data_preprocess']['use_read_write_operation_feature']

    if read_write_bool:

        x_read, y_read, x_write, y_write = read_write(mapping, read_write_bool)

        plt.figure(figsize=(16, 16))
        plt.scatter(x_read, y_read, c="b")
        plt.scatter(x_write, y_write, c="r", alpha=0.3)
        plotname = "plot_ft_" + create_filename(config) + ".png"
        plt.savefig(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                                 config['tsne']['plot_save_path'], plotname)))

        plt.figure(figsize=(16, 16))
        plt.scatter(x_read, y_read, c="b")
        plotname = "plot_ft_" + create_filename(config) + "_read.png"
        plt.savefig(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                                 config['tsne']['plot_save_path'], plotname)))

        plt.figure(figsize=(16, 16))
        plt.scatter(x_write, y_write, c="r")
        plotname = "plot_ft_" + create_filename(config) + "_write.png"
        plt.savefig(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                                 config['tsne']['plot_save_path'], plotname)))

    else:
        x, y = read_write(mapping, read_write_bool)
        plt.figure(figsize=(16, 16))
        plt.scatter(x, y, c="b")
        plotname = "plot_ft_" + create_filename(config) + ".png"
        plt.savefig(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                                 config['tsne']['plot_save_path'], plotname)))

    toc = time.time()
    my_logger.info(f"Process completed.Time taken for TSNE process to complete in seconds: {str(toc - tic)}")
