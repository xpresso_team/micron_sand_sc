from MulticoreTSNE import MulticoreTSNE as TSNE
from numpy import array
import os
import pickle
from app.create_filename_tsne import create_filename

cur_work_dir = os.getcwd()


def label_coordinate_mapping(config, master_path, labels, new_values):
    mapping = dict()
    for i in range(len(new_values)):
        mapping[labels[i]] = new_values[i]
    mapping_name = "mapping" + create_filename(config) + ".pickle"
    with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                           config['tsne']['plot_save_path'], "./mapping/", mapping_name)),
              'wb') as handle:
        pickle.dump(mapping, handle, protocol=pickle.HIGHEST_PROTOCOL)

    return mapping


def tsne_plot(config, master_path, model, my_logger):
    my_logger.info("Starting the TSNE process")

    '''Creates and TSNE model and plots it'''

    labels = []
    tokens = []

    ft_words = model.get_words()
    # print(len(ft_words))
    for i, word in enumerate(ft_words):
        tokens.append(model.get_word_vector(word))
        labels.append(word)

    tsne_token = array(tokens)
    tsne_model = TSNE(n_jobs=config['tsne']['cores'], perplexity=config['tsne']['perplexity'],
                      n_components=config['tsne']['n_components'], init='random', n_iter=config['tsne']['iter'],
                      random_state=23)
    new_values = tsne_model.fit_transform(tsne_token)
    my_logger.info("TSNE Process completed")

    new_values = new_values.tolist()

    my_logger.info("Saving TSNE results")
    mapping = label_coordinate_mapping(config, master_path, labels, new_values)
    my_logger.info("Results saved")

    return mapping
